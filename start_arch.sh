qemu-system-x86_64 \
-m 6124 \
-display default,show-cursor=on \
-usb -device usb-tablet \
-cpu Penryn,kvm=on,vendor=GenuineIntel \
-machine accel=hvf \
-drive file=arch.qcow2,if=virtio \
-smp 6 \
-vga virtio
