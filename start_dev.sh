qemu-system-x86_64 \
-m 6124 \
-show-cursor \
-usb -device usb-tablet \
-cpu Penryn,kvm=on,vendor=GenuineIntel \
-machine accel=hvf \
-drive file=dev.qcow2,if=virtio \
-smp 6 \
-vga virtio
