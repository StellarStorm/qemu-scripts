qemu-system-x86_64 \
-m 4096 \
-show-cursor \
-usb -device usb-tablet \
-cpu Nehalem,kvm=on,vendor=GenuineIntel \
-machine accel=hvf \
-drive file=debian_sid.qcow2,if=virtio \
-smp 4 \
-vga virtio